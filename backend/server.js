const express = require('express')
const getCollection = require('./dbConnection');
const cros = require("cors")


const app = express()
const PORT = 5000

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cros())

const collection = getCollection();


app.get('/testRoute', async (req, res) => {
  const result= await collection.insertOne(
    {
      "name":"avinash"
    }
  )
  res.end('Hello from Server!')
})

app.post('/signUp')
// , async (req,res) =>{
//   var registerFormData = req.body;
//   console.log(registerFormData);
//   collection.insertOne(registerFormData);
//   res.status(200).json(registerFormData);
// }
app.post('/signIn')
app.get('/profile')


app.listen(PORT, () => {
  console.log(`Node.js App running on port ${PORT}...`)
})
