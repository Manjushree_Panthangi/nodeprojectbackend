const { MongoClient } = require("mongodb");

const connectionString = `mongodb://localhost:27017`
const dbName = "userDetails";
const client = new MongoClient(connectionString);



const dbConnect=async () => {
    await client.connect();
    console.log("Connected successfully to MongoDB");
};

const getCollection = () =>{
  dbConnect();
  const db = client.db(dbName);
  const collection = db.collection('userDetails');
  return collection;
};

module.exports =getCollection;